(function (root, factory) {
  'use strict';
  /* istanbul ignore next */
  if (typeof define === 'function' && define.amd) {
    var b64encode = function (str) {
      return root.btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    };
    define(['xmlhttprequest', 'js-base64'], function () {
      // THis code fix widget file upload
      return (root.Github = factory(window.XMLHttpRequest, b64encode));
    });
  } else if (typeof module === 'object' && module.exports) {
    if (typeof window !== 'undefined') { // jscs:ignore
      module.exports = factory(window.XMLHttpRequest, window.btoa);
    } else { // jscs:ignore
      module.exports = factory(require('xmlhttprequest').XMLHttpRequest, require('js-base64').Base64.encode);
    }
  } else {
    var b64encode = function (str) {
      return root.btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    };
    root.WSapi = factory(root.XMLHttpRequest, b64encode);
  }
}(this, function (XMLHttpRequest, b64encode) {
  'use strict';
  var WSapi = function (startOptions) {
    var options = startOptions;
    var API_URL = options.apiUrl + '/v1';
    var _request = WSapi._request = function _request(method, path, data, cb, raw, sync) {
      function getURL() {
        var url = API_URL + path;

        //url += ((/\?/).test(url) ? '&' : '?');

        if (data && typeof data === 'object' && ['GET', 'HEAD', 'DELETE'].indexOf(method) > -1) {
          for (var param in data) {
            if (data.hasOwnProperty(param))
              url += '&' + encodeURIComponent(param) + '=' + encodeURIComponent(data[param]);
          }
        }

        return url;
      }

      var xhr = new XMLHttpRequest();

      xhr.open(method, getURL(), !sync);

      if (!sync) {
        xhr.onreadystatechange = function () {
          if (this.readyState === 4) {
            if (this.status >= 200 && this.status < 300 || this.status === 304) {
              cb(null, this.responseText ? JSON.parse(this.responseText) : true, this);
            } else {
              var reSend = function () {
                _request(method, path, data, cb, raw, sync);
              };
              cb({
                path: path,
                request: this,
                reSend: reSend,
                error: this.status
              });
            }
          }
        };
      }

      if (!raw) {
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
      } else {
        //xhr.setRequestHeader('Content-Type', undefined);
      }

      if ((options.token) || (options.username && options.password)) {
        var authorization = options.token ? 'Bearer ' + options.token :
        'Basic ' + b64encode(options.username + ':' + options.password);

        xhr.setRequestHeader('Authorization', authorization);
      }
      if (raw && data) {
        xhr.send(data);
      } else if (data) {
        xhr.send(JSON.stringify(data));
      } else {
        xhr.send();
      }

      if (sync) {
        return xhr.response;
      }
    };
    var _requestAllPages = WSapi._requestAllPages = function _requestAllPages(path, cb) {
      var results = [];

      (function iterate() {
        _request('GET', path, null, function (err, res, xhr) {
          if (err) {
            return cb(err);
          }

          results.push.apply(results, res);

          var links = (xhr.getResponseHeader('link') || '').split(/\s*,\s*/g);
          var next = null;

          links.forEach(function (link) {
            next = /rel="next"/.test(link) ? link : next;
          });

          if (next) {
            next = (/<(.*)>/.exec(next) || [])[1];
          }

          if (!next) {
            cb(err, results);
          } else {
            path = next;
            iterate();
          }
        });
      })();
    };
    var _urlParams = function (params) {
      if (params) {
        var urlParams = [];
        for (var key in params) {
          if (params.hasOwnProperty(key) && params[key] !== "") {
            urlParams.push(key + "=" + encodeURIComponent(params[key]));
          }
        }
        return '?' + urlParams.join('&');
      } else {
        return '';
      }
    };

    WSapi.setOptions = function (newOptions) {
      options = newOptions;
    };

    // "/company"

    /*get Companies, with paging
     */
    WSapi.getCompanies = function (params, cb) {
      _request('GET', '/company' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/billing"

    /*update company billing info
     */
    WSapi.updateCompanyBillingInfo = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/billing', params.body, cb);
    };

    // "/company_lead"
    WSapi.getCompanyLeads = function (params, cb) {
      _request('GET', '/company_lead' + _urlParams(params.params), null, cb);
    };

    // /company_lead/{companyId}
    WSapi.updateCompanyLead = function (params, cb) {
      _request('POST', '/company_lead/' + params.companyId + _urlParams(params.params), params.body, cb);
    };
    WSapi.getCompanyLead = function (params, cb) {
      _request('GET', '/company_lead/' + params.companyId + _urlParams(params.params), null, cb);
    }

    // "/user/{id}"

    /*remove user from all companies
     */
    WSapi.removeUserFromCompanies = function (params, cb) {
      _request('DELETE', '/user/' + params.userId, null, cb);
    };

    // /su/user/{userId}

    /* update user by su
     */
    WSapi.updateUserSu = function (params, cb) {
      _request('POST', '/su/user/' + params.userId, params.body, cb);
    };

    // "/user"

    /*get user, with paging
     */
    WSapi.getUsers = function (params, cb) {
      _request('GET', '/user' + _urlParams(params.params), null, cb);
    };

    // /widget

    /* create widget
     */
    WSapi.createWidget = function (params, cb) {
      _request('POST', '/widget', params.body, cb);
    };

    // /widget/{widgetId}

    /* update widget
     */
    WSapi.updateWidget = function (params, cb) {
      _request('POST', '/widget/' + params.widgetId, params.body, cb);
    };

    // /widget/{widgetId}/file

    /* upload widget file zip
     */
    WSapi.uploadWidget = function (params, cb) {
      _request('POST', '/widget/' + params.widgetId + '/file', params.body, cb, true);
    };

    // /widget/{widgetId}/image

    /* upload widget file image
     */
    WSapi.uploadWidgetImage = function (params, cb) {
      _request('POST', '/widget/' + params.widgetId + '/image' + _urlParams(params.params), params.body, cb, true);
    };

    // /company/{companyId}/branch/{branchId}/statistics

    /* post company branches rebuild Statistics
     */
    WSapi.rebuildStatisticsBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + "/statistics" + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/statistics/month

    /* post company branches rebuild Statistics for month
     */
    WSapi.rebuildStatisticsBranchMonth = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + "/statistics/month" + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/statistics/

    /* post company rebuild Statistics
     */
    WSapi.rebuildStatistics = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/statistics' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/features/add"
    /* change features add
     */
    WSapi.addCompanyFeatures = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/features/add', params.body, cb);
    };

    // "/company/{companyId}/features/remove"
    /* change features remove
     */
    WSapi.removeCompanyFeatures = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/features/remove', params.body, cb);
    };

    return WSapi;
  };
  return WSapi;
}));
